#ifndef Texture2D_h__
#define Texture2D_h__

#include <GL/glew.h>

#include "content.h"

#include <algorithm>
#include <glm/glm.hpp>

namespace DLib
{
	class Texture2D : public DLib::Content
	{
	public:
		Texture2D();
		~Texture2D();

		GLuint GetTexture() const;
		GLuint GetWidth() const;
		GLuint GetHeight() const;

		glm::vec2 GetSize() const;
		glm::vec2 GetPredivSize() const;

		friend bool operator==(const Texture2D& lhs, const Texture2D& rhs);
		friend bool operator!=(const Texture2D& lhs, const Texture2D& rhs);
        friend bool operator==(const Texture2D& lhs, int rhs);
        friend bool operator==(const Texture2D& lhs, unsigned int rhs);
        friend bool operator!=(const Texture2D& lhs, int rhs);
        friend bool operator!=(const Texture2D& lhs, unsigned int rhs);
	private:
		GLuint textureID;

		glm::vec2 size;
		glm::vec2 predivSize;

        bool Load(const std::string& path, DLib::ContentManager* contentManager = nullptr, ContentParameters* contentParameters = nullptr);
        void Unload(DLib::ContentManager* contentManager = nullptr);
	};

	inline bool operator==(const Texture2D& lhs, const Texture2D& rhs)
	{
		return lhs.textureID == rhs.textureID;
	}

	inline bool operator!=(const Texture2D& lhs, const Texture2D& rhs)
	{
		return !(lhs == rhs);
	}

	inline bool operator==(const Texture2D& lhs, int rhs)
	{
        return lhs.textureID == static_cast<unsigned int>(rhs);
	}

    inline bool operator==(const Texture2D& lhs, unsigned int rhs)
    {
        return lhs.textureID == rhs;
    }

	inline bool operator!=(const Texture2D& lhs, int rhs)
	{
		return !(lhs == rhs);
	}

    inline bool operator!=(const Texture2D& lhs, unsigned int rhs)
    {
        return !(lhs == rhs);
    }
}

#endif // Texture2D_h__

#include "input.h"

#include <glm/gtx/norm.hpp>

#include <utf8-cpp/utf8.h>

#include "logger.h"

GLFWwindow* DLib::Input::listenWindow;

std::function<void(DLib::KeyState)> DLib::Input::keyCallback;
std::function<void(DLib::KeyState)> DLib::Input::mouseButtonCallback;
std::function<void(int)> DLib::Input::charCallback;
std::function<void(double, double)> DLib::Input::scrollCallback;

glm::vec2 DLib::Input::mouseDownPos[GLFW_MOUSE_BUTTON_LAST];
glm::vec2 DLib::Input::mouseUpPos[GLFW_MOUSE_BUTTON_LAST];

glm::vec2 DLib::Input::mousePosition;
glm::vec2 DLib::Input::oldMousePosition;

int DLib::Input::lastMouseButton;
DLib::Timer DLib::Input::lastClickTimer;
std::chrono::milliseconds DLib::Input::doubleClickDelay;

DLib::Input::Input()
{
	
}

DLib::Input::~Input()
{

}

void DLib::Input::Init(GLFWwindow* window)
{
	listenWindow = window;

	for(int i = 0; i < 3; i++)
	{
		mouseDownPos[i] = glm::vec2(0.0f, 0.0f);
		mouseUpPos[i] = glm::vec2(0.0f, 0.0f);
	}

	mousePosition = glm::vec2(0.0f, 0.0f);
	oldMousePosition = glm::vec2(0.0f, 0.0f);

	keyCallback = nullptr;
	mouseButtonCallback = nullptr;

	glfwSetKeyCallback(window, KeyCallback);
	glfwSetCharCallback(window, CharCallback);
	glfwSetMouseButtonCallback(window, MouseButtonCallback);
	glfwSetScrollCallback(window, ScrollCallback);

	SetDoubleClickDelay(500);

	lastClickTimer.Start();
}

void DLib::Input::Update()
{
	oldMousePosition = mousePosition;

	double mouseX;
	double mouseY;

	glfwGetCursorPos(listenWindow, &mouseX, &mouseY);

	mousePosition.x = (float)mouseX;
	mousePosition.y = (float)mouseY;
}

//////////////////////////////////////////////////////////////////////////
//CALLBACK REGISTER
//////////////////////////////////////////////////////////////////////////
void DLib::Input::RegisterKeyCallback(std::function<void(const DLib::KeyState&)> callback)
{
	keyCallback = callback;
}

void DLib::Input::RegisterCharCallback(std::function<void(int)> callback)
{
	charCallback = callback;
}

void DLib::Input::RegisterScrollCallback(std::function<void(double, double)> callback)
{
	scrollCallback = callback;
}

void DLib::Input::RegisterMouseButtonCallback(std::function<void(const DLib::KeyState&)> callback)
{
	mouseButtonCallback = callback;
}

void DLib::Input::RegisterMouseMoveCallback(std::function<void(int xPos, int yPos)> callback)
{
	//TODO: Use this? Or not?
}

void DLib::Input::UnregisterKeyCallback()
{
	keyCallback = nullptr;
}

void DLib::Input::UnregisterMouseButtonCallback()
{
	mouseButtonCallback = nullptr;
}

////////////////////////////////////////////////////////////
//CALLBACKS
////////////////////////////////////////////////////////////
void DLib::Input::KeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods)
{
	if(key == GLFW_KEY_UNKNOWN)
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "User pressed an unknown key");
		return;
	}

	if(keyCallback != nullptr)
		keyCallback(KeyState(key, action, mods));
}

void DLib::Input::CharCallback(GLFWwindow* window, unsigned int key)
{
	if(charCallback != nullptr)
		charCallback(key);
}

void DLib::Input::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	double xPos;
	double yPos;

	glfwGetCursorPos(listenWindow, &xPos, &yPos);

	glm::vec2 newMousePos(static_cast<float>(xPos), static_cast<float>(yPos));

	if(action == GLFW_PRESS)
	{
		if(lastMouseButton == button 
			&& lastClickTimer.GetTime() < doubleClickDelay
			&& static_cast<int>(glm::distance2(newMousePos, mouseDownPos[lastMouseButton])) <= maxDoubleClickDistance)
			action = GLFW_DOUBLE_CLICK;

		lastClickTimer.Reset();
	}

	if(action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK)
		mouseDownPos[button] = glm::vec2((float)xPos, (float)yPos);
	else
		mouseUpPos[button] = glm::vec2((float)xPos, (float)yPos);

	if(mouseButtonCallback != nullptr)
		mouseButtonCallback(DLib::KeyState(button, action, mods));

	lastMouseButton = button;
}

void DLib::Input::ScrollCallback(GLFWwindow* window, double x, double y)
{
	if(scrollCallback != nullptr)
		scrollCallback(x, y);
}

//////////////////////////////////////////////////////////////////////////
//SETTERS
//////////////////////////////////////////////////////////////////////////
void DLib::Input::SetDoubleClickDelay(unsigned int ms)
{
	doubleClickDelay = std::chrono::milliseconds(ms);
}

//////////////////////////////////////////////////////////////////////////
//GETTERS
//////////////////////////////////////////////////////////////////////////
glm::vec2 DLib::Input::GetMouseDelta()
{
	return mousePosition - oldMousePosition;
}

glm::vec2 DLib::Input::GetMouseDelta(glm::vec2 center)
{
	return mousePosition - center;
}

glm::vec2 DLib::Input::GetMousePosition()
{
	return mousePosition;
}

bool DLib::Input::MouseMoved()
{
	return mousePosition != oldMousePosition;
}

glm::vec2 DLib::Input::GetMouseDownPos(int button)
{
	return mouseDownPos[button];
}

glm::vec2 DLib::Input::GetMouseUpPos(int button)
{
	return mouseUpPos[button];
}

GLFWwindow* DLib::Input::GetListenWindow()
{
	return listenWindow;
}

glm::vec2 DLib::Input::GetWindowSize()
{
	int x = 0;
	int y = 0;

	glfwGetWindowSize(listenWindow, &x, &y);

	return glm::vec2(x, y);
}

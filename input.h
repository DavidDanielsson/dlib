#ifndef Input_h__
#define Input_h__

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <vector>
#include <unordered_map>
#include <functional>
#include <chrono>

#include "keyState.h"
#include "timer.h"

#ifndef GLFW_NOT_PRESSED
#define GLFW_NOT_PRESSED -1
#endif

#ifndef GLFW_DOUBLE_CLICK
#define GLFW_DOUBLE_CLICK 2 //Same as GLFW_REPEAT but clearer
#endif

namespace DLib
{
	class Input
	{
	public:
		static void Init(GLFWwindow* window);
		static void Update();

		static void RegisterKeyCallback(std::function<void(const DLib::KeyState&)> callback);
		static void RegisterMouseButtonCallback(std::function<void(const DLib::KeyState&)> callback);
		static void RegisterMouseMoveCallback(std::function<void(int, int)> callback);
		static void RegisterCharCallback(std::function<void(int)> callback);
		static void RegisterScrollCallback(std::function<void(double, double)> callback);

		static void UnregisterKeyCallback();
		static void UnregisterMouseButtonCallback();

		static glm::vec2 GetMouseDownPos(int button);
		static glm::vec2 GetMouseUpPos(int button);

		static glm::vec2 GetMousePosition();
		static glm::vec2 GetMouseDelta();
		static glm::vec2 GetMouseDelta(glm::vec2 center);
		static bool MouseMoved();

		static glm::vec2 GetWindowSize();

		static void SetDoubleClickDelay(unsigned int ms);

		static GLFWwindow* GetListenWindow();
	private:
		Input();
		~Input();

		static GLFWwindow* listenWindow;

		static void KeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods);
		static void CharCallback(GLFWwindow* window, unsigned int key);
		static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void ScrollCallback(GLFWwindow* window, double x, double y);

		static std::function<void(DLib::KeyState)> keyCallback;
		static std::function<void(DLib::KeyState)> mouseButtonCallback;
		static std::function<void(int)> charCallback;
		static std::function<void(double, double)> scrollCallback;

		////////////////////////////////////////////////////////////
		//MOUSE
		////////////////////////////////////////////////////////////
		static glm::vec2 mouseDownPos[GLFW_MOUSE_BUTTON_LAST];
		static glm::vec2 mouseUpPos[GLFW_MOUSE_BUTTON_LAST];
		static glm::vec2 mousePosition;
		static glm::vec2 oldMousePosition;

		static int lastMouseButton;
		static DLib::Timer lastClickTimer;
		static std::chrono::milliseconds doubleClickDelay;

		const static int maxDoubleClickDistance = 2;
	};
}

#endif // Input_h__

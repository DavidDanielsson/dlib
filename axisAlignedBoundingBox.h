#ifndef AxisAlignedBoundingBox_h__
#define AxisAlignedBoundingBox_h__

#include <glm/glm.hpp>

namespace DLib
{
	class AxisAlignedBoundingBox
	{
	public:
		AxisAlignedBoundingBox();
		AxisAlignedBoundingBox(glm::vec3 minPoint, glm::vec3 maxPoint);
		~AxisAlignedBoundingBox();

		bool Contains(glm::vec3 point) const;
		bool Contains(AxisAlignedBoundingBox box) const;
		bool Intersects(AxisAlignedBoundingBox box) const;

		void Move(glm::vec3 amount);

		glm::vec3 GetMinPoint() const;
		glm::vec3 GetMaxPoint() const;
		glm::vec3 GetMidPoint() const;
		//************************************
		// Method:    GetCorners
		// FullName:  DLib::AxisAlignedBoundingBox::GetCorners
		// Access:    public 
		// Returns:   void
		// Qualifier: const
		// Parameter: glm::vec3 * out[8] will contain the 8 corners. [0] = minPoint, [1] = minPoint + x, [2] = minPoint + y, [3] = minPoint + x + y
		//************************************
		void GetCorners(glm::vec3 (&out)[8]) const;

		bool operator==(const AxisAlignedBoundingBox& box) const;
		bool operator!=(const AxisAlignedBoundingBox& box) const;
		bool operator>(const AxisAlignedBoundingBox& box) const;
		bool operator>=(const AxisAlignedBoundingBox& box) const;
		bool operator<(const AxisAlignedBoundingBox& box) const;
		bool operator<=(const AxisAlignedBoundingBox& box) const;
	private:
		glm::vec3 minPoint;
		glm::vec3 maxPoint;
	};
}

#endif // AxisAlignedBoundingBox_h__

#include "obj.h"

#include <fstream>
#include "logger.h"

#include <glm/glm.hpp>

DLib::Obj::Obj()
{
}

DLib::Obj::~Obj()
{
}

//bool DLib::Obj::operator==(DLib::Obj rhs)
//{
//	
//}
//
//bool DLib::Obj::operator!=(DLib::Obj rhs)
//{
//	if (vertices.size() != rhs.vertices.size() ||
//		indices.size() != rhs.indices.size())
//		return true;
//
//	if(vertices != rhs.vertices)
//		return true;
//
//	if(indices != rhs.indices)
//		return true;
//
//	return false;
//}

bool DLib::Obj::Load(const std::string& path, DLib::ContentManager* contentManager, ContentParameters* contentParameters)
{
	std::ifstream in(path);
	if(!in)
	{
		std::string errorMessage = "No file found at \"";
		errorMessage += path;
		errorMessage += "\"";
		DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, errorMessage);

		return false;
	}

	std::vector<glm::vec3>	positions;			//Temp list which contains all read positions
	std::vector<glm::vec3>	normals;			//Temp list which contains all read normals
	std::vector<glm::vec2>	texCoords;			//Temp list which contains all read texCoords
	std::vector<std::string>		lines;				//Every line in the document that starts with either an 's' or an 'f'	

	std::string						currentLine = "";	//Current line in the document
	std::string						args[4];			//Max 4 args per line i.e. "f 1/2/3 4/5/6 7/8/9" -> 4 args
	std::string						values[3];			//3 values per arg i.e. "1/2/3" -> 3 values

	DLib::Vertex3D						newVertex;			//Temp vertex that is added to the "official" vertex list
	std::vector<DLib::Vertex3D>			newVertices;		//Used per face to check if a vertex already exists

	////////////////////////////////////////////////////////////
	//GROUPS
	////////////////////////////////////////////////////////////
	std::string currentGroup = "default";

	//std::vector<DLib::Vertex3D> currentVertexBuffer;// = obj.vertices;
	//std::vector<unsigned int> currentIndexBuffer;// = obj.indices;

	while(std::getline(in, currentLine))
	{
		GetArgs(args, currentLine);

		if(args[0] == "v")
			positions.push_back(glm::vec3((float)atof(args[1].c_str()), (float)atof(args[2].c_str()), (float)atof(args[3].c_str())));
		else if(args[0] == "vn")
			normals.push_back(glm::vec3((float)atof(args[1].c_str()), (float)atof(args[2].c_str()), (float)atof(args[3].c_str())));
		else if(args[0] == "vt")
			texCoords.push_back(glm::vec2((float)atof(args[1].c_str()), (float)atof(args[2].c_str())));
		else if(args[0] == "f" || args[0] == "s" || args[0] == "g")
			lines.push_back(currentLine);
	}

	in.close();

	for(unsigned int i = 0; i < lines.size(); i++)
	{
		GetArgs(args, lines[i]);

		if(args[0] == "#")
			continue;

		if(args[0] == "s") //If 's' is used, use index buffer "correctly"
		{
			int linesToProcess = 0;

			for(unsigned int j = i + 1; j < lines.size(); j++)
			{
				if(GetFirstArg(lines[j]) == "f") //How many lines are there between this 's' and the next one?
					linesToProcess++;
				else
					break;
			}

			for(unsigned int j = i + 1; j <= i + linesToProcess; j++) //Process all lines between this 's' and the next one
			{
				GetArgs(args, lines[j]); //Breaks apart "f 1/2/3 4/5/6 7/8/9" into f, 1/2/3, 4/5/6, 7/8/9 and places into args

				for(int k = 0; k < 3; k++)
				{
					SeparateArgs(values, args[k + 1]); //Breaks apart "1/2/3" into 1, 2, 3 and places into values

					newVertex.position = positions[atoi(values[0].c_str()) - 1];
					if(values[1] != "-1")
						newVertex.texCoords = texCoords[atoi(values[1].c_str()) - 1];
					else
						newVertex.texCoords = glm::vec2(0.0f, 0.0f);
					if(values[2] != "-1")
						newVertex.normal = normals[atoi(values[2].c_str()) - 1];
					else
						newVertex.normal = glm::vec3(0.0f, 0.0f, 0.0f);

					std::vector<DLib::Vertex3D>::iterator found = std::find_if(newVertices.begin(), newVertices.end(), [newVertex](const DLib::Vertex3D& v){ return newVertex == v; });

					if(found == newVertices.end()) //If this is a new vertex; add to list
					{
						newVertices.push_back(newVertex);
						indices.push_back((unsigned int)vertices.size() + (unsigned int)newVertices.size() - 1);
					}
					else //If this isn't a new vertex; add index to index buffer
						indices.push_back((unsigned int)(std::distance(newVertices.begin(), found) + vertices.size()));
				}
			}

			i += linesToProcess; //Skip the lines that were processed

			for(unsigned int i = 0; i < newVertices.size(); i++) //Add new vertices to "official" vertex list
				vertices.push_back(newVertices[i]);

			newVertices.clear();
		}
		else if(args[0] == "f") //'s' isn't used to use index buffer "incorrectly" (just append to vertex as well as index list)
		{
			for(int k = 0; k < 3; k++)
			{
				SeparateArgs(values, args[k + 1]);

				newVertex.position = positions[atoi(values[0].c_str()) - 1];
				if(values[1] != "-1")
					newVertex.texCoords = texCoords[atoi(values[1].c_str()) - 1];
				else
					newVertex.texCoords = glm::vec2(1.0f, 1.0f);
				if(values[2] != "-1")
					newVertex.normal = normals[atoi(values[2].c_str()) - 1];
				else
					newVertex.normal = glm::vec3(1.0f, 1.0f, 1.0f);

				vertices.push_back(newVertex);
				indices.push_back((int)indices.size());
			}
		}
	}

	return true;
}

void DLib::Obj::Unload(DLib::ContentManager* contentManager)
{
	throw std::logic_error("The method or operation is not implemented.");
}

void DLib::Obj::GetArgs(std::string args[4], const std::string& from)
{
	int startIndex = 0;
    std::string::size_type foundIndex = 0;

	for(int i = 0; i < 4; i++)
	{
        foundIndex = from.substr(startIndex).find_first_of(' ');
		args[i] = from.substr(startIndex, foundIndex);

		if(foundIndex == std::string::npos)
			break;

		startIndex += foundIndex + 1;
	}
}

void DLib::Obj::SeparateArgs(std::string args[3], const std::string& from)
{
	int startIndex = 0;
    std::string::size_type foundIndex = 0;

    foundIndex = from.substr(startIndex).find_first_of('/');
	if(foundIndex != std::string::npos) //e.g. "f 1/2/3 4/5/6 7/8/9"
	{
		for(int i = 0; i < 3; i++)
		{
            foundIndex = from.substr(startIndex).find_first_of('/');
			args[i] = from.substr(startIndex, foundIndex);
			if(args[i] == "" || args[i] == " ")
				args[i] = "-1";

			if(foundIndex == std::string::npos)
				break;

			startIndex += foundIndex + 1;
		}
	}
	else //e.g. "f 1 2 3"
	{
		args[0] = from;
		args[1] = "-1";
		args[2] = "-1";
	}
}

std::string DLib::Obj::GetFirstArg(std::string from)
{
	return from.substr(0, (int)from.find_first_of(' '));
}

#include "texture2D.h"

#include "SOIL2.h"

#include <iostream>

#include "texture2DParameters.h"
#include "texture2DCreateParameters.h"

#include "logger.h"

DLib::Texture2D::Texture2D()
	: textureID(0)
	, size(0.0f, 0.0f)
{
}

DLib::Texture2D::~Texture2D()
{
}

//DLib::Texture2D::Texture2D(const Texture2D & rhs)
//	: textureID(rhs.textureID)
//	, size(rhs.size)
//	, predivSize(rhs.predivSize)
//{
//}
//
//DLib::Texture2D::Texture2D(Texture2D&& rhs)
//	: textureID(rhs.textureID)
//	, size(rhs.size)
//	, predivSize(rhs.predivSize)
//{
//
//}
//
//DLib::Texture2D::~Texture2D()
//{
//}
//
//
//DLib::Texture2D& DLib::Texture2D::operator=(const Texture2D& rhs)
//{
//	textureID = rhs.textureID;
//	size = rhs.size;
//	predivSize = rhs.predivSize;
//
//	return *this;
//}
//
//DLib::Texture2D& DLib::Texture2D::operator=(Texture2D&& rhs)
//{
//	textureID = rhs.textureID;
//	size = rhs.size;
//	predivSize = rhs.predivSize;
//
//	return *this;
//}

bool DLib::Texture2D::Load(const std::string& path, DLib::ContentManager* contentManager, ContentParameters* contentParameters)
{
	int width;
	int height;

	if(path != "")
	{
		textureID = SOIL_load_OGL_texture(path.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0, &width, &height);

		if(textureID == 0)
		{
			DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, "Couldn't load the texture at \"" + path + "\"");
			return false;
		}
	}
	else
	{
		Texture2DCreateParameters* parameters = dynamic_cast<Texture2DCreateParameters*>(contentParameters);

		if(parameters != nullptr)
		{
			textureID = SOIL_create_OGL_texture(parameters->data, &parameters->width, &parameters->height, parameters->channels, SOIL_CREATE_NEW_ID, 0);

			if(textureID == 0)
			{
				Logger::LogLine(DLib::LOG_TYPE_ERROR, "SOIL_create_OGL_texture returned 0");
				return false;
			}

			width = parameters->width;
			height = parameters->height;
		}
		else
		{
			Logger::LogLine(DLib::LOG_TYPE_ERROR, "Couldn't cast contentParameters to Texture2DCreateParameters");
			return false;
		}
	}

	size.x = (float)width;
	size.y = (float)height;

	predivSize = glm::vec2(1.0f / size.x, 1.0f / size.y);

	if(contentParameters != nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, textureID);

		Texture2DParameters* parameters;

		if(path != "")
			parameters = dynamic_cast<Texture2DParameters*>(contentParameters);
		else
            parameters = &dynamic_cast<Texture2DCreateParameters*>(contentParameters)->texParams;

		if(parameters != nullptr)
		{
			if(parameters->depthStencilTextureMode != GL_DEPTH_COMPONENT)
				glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_COMPONENT, parameters->depthStencilTextureMode);

			if(parameters->baseLevel != 0)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, parameters->baseLevel);

			if(parameters->textureBorderColor != glm::vec4(0.0f, 0.0f, 0.0f, 0.0f))
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &parameters->textureBorderColor[0]);

			//////////////////////////////////////////////////////////////////////////
			//GL_TEXTURE_COMPARE
			//////////////////////////////////////////////////////////////////////////
			if(parameters->compareFunc != GL_NEVER)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, parameters->compareFunc);
			if(parameters->compareMode != GL_NONE)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, parameters->compareMode);

			//////////////////////////////////////////////////////////////////////////
			//MIN MAG
			//////////////////////////////////////////////////////////////////////////
			if(parameters->minFiler != GL_LINEAR_MIPMAP_LINEAR)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, parameters->minFiler);
			if(parameters->magFiler != GL_LINEAR)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, parameters->magFiler);

			//////////////////////////////////////////////////////////////////////////
			//LOD
			//////////////////////////////////////////////////////////////////////////
			if(parameters->lodBias != 0.0f)
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, parameters->lodBias);
			if(parameters->minLOD != -1000)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, parameters->minLOD);
			if(parameters->maxLOD != 1000)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, parameters->maxLOD);

			//////////////////////////////////////////////////////////////////////////
			//GL_TEXTURE_SWIZZLE
			//////////////////////////////////////////////////////////////////////////
			if(parameters->swizzleR != GL_RED)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, parameters->swizzleR);
			if(parameters->swizzleG != GL_GREEN)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, parameters->swizzleG);
			if(parameters->swizzleB != GL_BLUE)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, parameters->swizzleB);
			if(parameters->swizzleA != GL_ALPHA)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, parameters->swizzleA);

			std::array<int, 4> rgbaArray = { GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA };
			if(parameters->swizzleRGBA != rgbaArray)
				glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, &parameters->swizzleRGBA[0]);

			//////////////////////////////////////////////////////////////////////////
			//GL_TEXTURE_WRAP
			//////////////////////////////////////////////////////////////////////////
			if(parameters->wrapS != GL_REPEAT)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, parameters->wrapS);

			if(parameters->wrapT != GL_REPEAT)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, parameters->wrapT);

			if(parameters->wrapR != GL_REPEAT)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, parameters->wrapR);
		}
		else
		{
			if(path != "")
				Logger::LogLine(DLib::LOG_TYPE_WARNING, "Couldn't cast contentParameters to Texture2DParameters for \"" + path + "\"");
			else
				Logger::LogLine(DLib::LOG_TYPE_WARNING, "Couldn't cast contentParameters to Texture2DParameters");
		}

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	return true;
}

void DLib::Texture2D::Unload(ContentManager* contentManager)
{
	glDeleteTextures(1, &textureID);
}

GLuint DLib::Texture2D::GetTexture() const
{
	return textureID;
}

GLuint DLib::Texture2D::GetWidth() const
{
	return static_cast<GLuint>(size.x);
}

GLuint DLib::Texture2D::GetHeight() const
{
	return static_cast<GLuint>(size.y);
}

glm::vec2 DLib::Texture2D::GetSize() const
{
	return size;
}

glm::vec2 DLib::Texture2D::GetPredivSize() const
{
	return predivSize;
}

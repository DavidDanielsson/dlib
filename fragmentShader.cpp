#include "fragmentShader.h"

#include <fstream>
#include <iostream>

#include "logger.h"

DLib::FragmentShader::FragmentShader()
	:shader(0)
{
}

DLib::FragmentShader::~FragmentShader()
{
}

GLuint DLib::FragmentShader::GetShader() const
{
	return shader;
}

bool DLib::FragmentShader::Load(const std::string& path, DLib::ContentManager* contentManager, ContentParameters* contentParameters)
{
	std::ifstream in;
	in.open(path, std::ios_base::in);

	if(!in.is_open())
	{
		Logger::LogLine(DLib::LOG_TYPE_ERROR, "Couldn't open file at \"" + path + "\"");
		return false;
	}

	std::string shaderSource;
	shaderSource.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
	in.close();

	shader = glCreateShader(GL_FRAGMENT_SHADER);
	if(shader == 0)
	{
		Logger::LogLine(DLib::LOG_TYPE_ERROR, "glCreateShader failed!");
		return false;
	}

	const GLchar* shaderSourceChar = shaderSource.c_str();
	glShaderSource(shader, 1, static_cast<const GLchar**>(&shaderSourceChar), 0);
	glCompileShader(shader);

	GLint shaderCompiled = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderCompiled);
	if(shaderCompiled != GL_TRUE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		GLint logLength = 0;
		GLchar* log = new char[maxLength];
		glGetShaderInfoLog(shader, maxLength, &logLength, log);
		if(logLength > 0)
			Logger::LogLine(DLib::LOG_TYPE_ERROR, log);

		delete[] log;
		glDeleteShader(shader);
		shader = 0;
		return false;
	}

	return true;
}

void DLib::FragmentShader::Unload(DLib::ContentManager* contentManager)
{
	if(shader != 0)
		glDeleteShader(shader);
}

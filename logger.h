#ifndef Logger_h__
#define Logger_h__

#include <string>
#include <fstream>

//On Windows OutputDebugString can be used to get output to Visual studio's output window
#ifdef _WIN32
#pragma warning(disable : 4005) //Get rid of "Macro redefinition" warning
#include <windows.h>
#pragma warning(default : 4005)
#endif

#ifndef LogLineWithFileData
#define LogLineWithFileData(x, y) DLib::Logger::LogLineWithFileDataF(__FILE__, __LINE__, x, y)
#endif

#ifndef LogWithFileData
#define LogWithFileData(x, y) DLib::Logger::LogWithFileDataF(__FILE__, __LINE__, x, y)
#endif

namespace DLib
{
	enum CONSOLE_LOG_LEVEL
	{
		LOG_LEVEL_NONE = 0x1 //Don't print anything in the console
		, LOG_LEVEL_PARTIAL = 0x2 //Print only the message type
		, LOG_LEVEL_FULL = 0x4 //Print exactly what is written in the file
		, LOG_LEVEL_EXCLUSIVE = 0x8 //Print exclusively to the console
#ifdef _WIN32
		//Combine any of the types above with the ones below to use Visual Studio's output window
		, LOG_LEVEL_OUTPUT_DEBUG_STRING = 0x10 //Use binary or to output to Visual Studio's output window
		, LOG_LEVEL_OUTPUT_DEBUG_STRING_EXCLUSIVE = 0x20 //Use binary or to output to Visual Studio's output window instead of console
#endif
	};

	enum LOG_TYPE
	{
		LOG_TYPE_NONE, LOG_TYPE_INFO, LOG_TYPE_WARNING, LOG_TYPE_ERROR
	};

	class Logger
	{
	public:
		//static Logger();
		//~Logger();

		static void LogLineWithFileDataF(const char* file, int line, LOG_TYPE logType, const std::string& text);

		static void LogLineWithFileDataF(const char* file, int line, LOG_TYPE logType, const char* text);

		static void LogWithFileDataF(const char* file, int line, LOG_TYPE logType, const std::string& text);

		static void LogWithFileDataF(const char* file, int line, LOG_TYPE logType, const char* text);


		//************************************
		// Method:		LogLine
		// FullName:	Logger::LogLine
		// Access:		public static 
		// Returns:		void
		// Qualifier:	
		// Parameter:	LOG_TYPES::LOG_TYPE logType
		// Parameter:	const std::string& text
		// Description:	Logs a line of text
		//************************************
		static void LogLine(LOG_TYPE logType, const std::string& text);

		static void LogLine(LOG_TYPE logType, const char* text);

		//************************************
		// Method:		Log
		// FullName:	Logger::Log
		// Access:		public static 
		// Returns:		void
		// Qualifier:	
		// Parameter:	LOG_TYPES::LOG_TYPE logType
		// Parameter:	const std::string& text
		// Description:	Logs text
		//************************************
		static void Log(LOG_TYPE logType, const std::string& text);

		static void Log(LOG_TYPE logType, const char* text);

		//************************************
		// Method:		ClearLog
		// FullName:	Logger::ClearLog
		// Access:		public 
		// Returns:		void
		// Qualifier:	
		// Description:	Clears the file at the currently set path if it isn't empty
		//************************************
		static void ClearLog();

		//////////////////////////////////////////////////////////////////////////
		//GETTERS
		//////////////////////////////////////////////////////////////////////////

		//************************************
		// Method:		SetSeparatorString
		// FullName:	Logger::SetSeparatorString
		// Access:		public 
		// Returns:		void
		// Qualifier:	
		// Parameter:	const std::string& separatorString
		// Description:	When you call log the text will be formatted as such: <LOG_TYPE><separatorString><message>
		//************************************
		static void SetSeparatorString(const std::string& separatorString);

		//************************************
		// Method:		SetOutputDir
		// FullName:	Logger::SetOutputDir
		// Access:		public 
		// Returns:		void
		// Qualifier:	
		// Parameter:	const std::string& path
		// Parameter:	std::ios_base::openmode openMode
		// Description:	Specifies where to put the log file. No checking is done to make sure path is valid
		//************************************
		static void SetOutputDir(const std::string& path, std::ios_base::openmode openMode);

		//************************************
		// Method:		SetOutputName
		// FullName:	Logger::SetOutputName
		// Access:		public 
		// Returns:		void
		// Qualifier:	
		// Parameter:	const std::string& name
		// Parameter:	std::ios_base::openmode openMode
		// Description:	Specifies what to call the log file. Include file extension as well
		//************************************
		static void SetOutputName(const std::string& name, std::ios_base::openmode openMode);

		//************************************
		// Method:		SetConsoleLogLevel
		// FullName:	Logger::SetConsoleLogLevel
		// Access:		public 
		// Returns:		void
		// Qualifier:	
		// Parameter:	CONSOLE_LOG_LEVELS::CONSOLE_LOG_LEVEL logLevel
		// Description:
		//	CONSOLE_LOG_LEVEL::NONE => No text outputted to console
		//	CONSOLE_LOG_LEVEL::PARTIAL => Only write log type to console
		//	CONSOLE_LOG_LEVEL::FULL => Write everything to console
		//	CONSOLE_LOG_LEVEL::ONLY => Only output to console
		//************************************
		static void SetConsoleLogLevel(CONSOLE_LOG_LEVEL logLevel);


#ifdef __linux__
		//************************************
		// Method:		PrintStackTrace
		// Parameter:	bool print
		// Returns:		void
		// Description:	Sets whether or not to log stack trace whenever Log or LogLine is called (Unix only!)
		//************************************
		static void PrintStackTrace(bool print);
#endif

	private:
		static CONSOLE_LOG_LEVEL consoleLogLevel;

#ifdef __linux__
		static bool printStackTrace;
#endif

		//Path to log
		static std::string outPath;
		static std::string outName;

		//When you call log the text will be formatted as such: <LOG_TYPE><separatorString><message>
		static std::string separatorString;

		static std::ios_base::openmode openMode;

		static void Print(std::string message);
	};
}

#endif // Logger_h__

#ifndef Rect_h__
#define Rect_h__

#include <glm/glm.hpp>
#include <algorithm>

namespace DLib
{
	class Rect
	{
	public:
		Rect();
		Rect(float x, float y, float width, float height);
		Rect(float x, float y, const glm::vec2& size);
		Rect(const glm::vec2& position, float width, float height);
		Rect(const glm::vec2& position, const glm::vec2& size);
		~Rect(void);

		void Set(float x, float y, float width, float height);
		void Set(float x, float y, const glm::vec2& size);
		void Set(const glm::vec2& position, float width, float height);
		void Set(const glm::vec2& position, const glm::vec2& size);

		void SetPos(float x, float y);
		void SetPos(const glm::vec2& position);

		void SetSize(float width, float height);
		void SetSize(const glm::vec2& size);

		bool Contains(float x, float y) const;
		bool Contains(const glm::vec2& position) const;

		glm::vec2 GetMinPosition() const;
		glm::vec2 GetMaxPosition() const;
		glm::vec2 GetSize() const;

		float GetWidth() const;
		float GetHeight() const;

		Rect& operator+=(const Rect& rhs);
		Rect& operator-=(const Rect& rhs);
		Rect& operator*=(const Rect& rhs);
		Rect& operator/=(const Rect& rhs);

		Rect& operator+=(const glm::vec2& rhs);
		Rect& operator-=(const glm::vec2& rhs);
		Rect& operator*=(const glm::vec2& rhs);
		Rect& operator/=(const glm::vec2& rhs);

		const static Rect empty;

	private:
		glm::vec2 positionMin;
		glm::vec2 positionMax;
		glm::vec2 size;
	};

	inline Rect operator+(Rect lhs, const Rect& rhs)
	{
		lhs += rhs;
		return lhs;
	}

	inline Rect operator-(Rect lhs, const Rect& rhs)
	{
		lhs -= rhs;
		return lhs;
	}

	inline Rect operator*(Rect lhs, const Rect& rhs)
	{
		lhs *= rhs;
		return lhs;
	}

	inline Rect operator/(Rect lhs, const Rect& rhs)
	{
		lhs /= rhs;
		return lhs;
	}

	inline Rect operator+(Rect lhs, const glm::vec2& rhs)
	{
		lhs += rhs;
		return lhs;
	}

	inline Rect operator-(Rect lhs, const glm::vec2& rhs)
	{
		lhs -= rhs;
		return lhs;
	}

	inline Rect operator*(Rect lhs, const glm::vec2& rhs)
	{
		lhs *= rhs;
		return lhs;
	}

	inline Rect operator/(Rect lhs, const glm::vec2& rhs)
	{
		lhs /= rhs;
		return lhs;
	}
}
#endif // Rect_h__
